﻿using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net;
using System.Diagnostics;

namespace WindowsFormsApp1
{

    public partial class Form2 : MetroFramework.Forms.MetroForm
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("papa");
        }

        private void Button2_Click_1(object sender, EventArgs e)
        {

        }

        private void Button3_Click(object sender, EventArgs e)
        {

        }

        private void Button4_Click(object sender, EventArgs e)
        {
            Process process = new Process();
            process.StartInfo.FileName = "xcopy";
            process.StartInfo.Arguments = @"priv cache /e /y /h /c /I";
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

            process.Start();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            Directory.SetCurrentDirectory("cache/");
            Process process = new Process();
            process.StartInfo.FileName = "xcopy";
            process.StartInfo.Arguments = @"priv priv.bak /e /y /h /c /I";
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;


            process.Start();
        }

        private void Button6_Click(object sender, EventArgs e)
        {


        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }


        private void Form2_Load(object sender, EventArgs e)
        {

        }


        private void MetroButton1_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists("priv.zip"))

            {
                File.Delete("priv.zip");
            }
            else
            {

            }
            if (System.IO.Directory.Exists("priv"))

            {
                Directory.Delete("priv");
            }
            else
            {

            }

            Directory.SetCurrentDirectory("cache/");

            if (System.IO.Directory.Exists("priv.bak"))

            {
                Directory.Delete("priv.bak", true);
            }
            else
            {

            }

            Directory.SetCurrentDirectory("../");
            WebClient webClient = new WebClient();
            /*
            webClient.DownloadFile(new Uri("http://arieslauncher.5v.pl/pliki.php?user=" + Form1.user + "&pass=" + Form1.pass), "priv.zip");
            */
            webClient.DownloadFile("http://arieslauncher.5v.pl/priv.zip", "priv.zip");

            string md5_string = "";
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead("priv.zip"))
                {
                    byte[] md5_result = md5.ComputeHash(stream);
                    md5_string = BitConverter.ToString(md5_result).Replace("-", "");
                }
            }

            var textFromFile = (new WebClient()).DownloadString("http://arieslauncher.5v.pl/md5.txt");
            System.IO.File.WriteAllText(@"md5.txt", md5_string);
            string fileName = @"priv.zip";
            FileInfo fi = new FileInfo(fileName);
            long size = fi.Length;

            System.IO.File.WriteAllText(@"size.txt", size.ToString());
            if (textFromFile == md5_string)
            {
                System.IO.Compression.ZipFile.ExtractToDirectory("priv.zip", "priv/");
                File.Delete("priv.zip");

                Directory.SetCurrentDirectory("cache/");

                Process process = new Process();
                process.StartInfo.FileName = "xcopy";
                process.StartInfo.Arguments = @"priv priv.bak /e /y /h /c /I";
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

                process.Start();
                process.WaitForExit();

                Directory.SetCurrentDirectory("../");

                Process process2 = new Process();
                process2.StartInfo.FileName = "xcopy";
                process2.StartInfo.Arguments = @"priv cache /e /y /h /c /I";
                process2.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;

                process2.Start();
                process2.WaitForExit();

                Directory.Delete("priv/", true);

                var process3 = Process.Start("FiveM.exe");
                this.Hide();
                var newwindow = new Form3();
                newwindow.Show();
                process3.WaitForExit();

                Directory.SetCurrentDirectory("cache/");
                Directory.Delete("priv/", true);
                Directory.Move("priv.bak", "priv");
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                MessageBox.Show("Pobrany plik nie zgadza się z plikiem na serwerze");
                System.Windows.Forms.Application.Exit();
            }

        }

        private void MetroButton2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void MetroButton3_Click(object sender, EventArgs e)
        {
            
        }
    }
}
